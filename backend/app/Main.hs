{-#LANGUAGE BlockArguments #-}
{-#LANGUAGE OverloadedStrings #-}
module Main(main) where

import HttpControl

import System.Random (randomIO)

import Data.Text (Text)
import qualified Data.Text.IO as TXTIO (readFile)

import Network.Wai (Request, Response, ResponseReceived)
import Network.Wai.Handler.Warp (run)
import qualified Network.Wai as WAI (pathInfo, requestHeaders)



main :: IO ()
main = do
  let port = 3000 in do
    js <- TXTIO.readFile "index.js"
    putStrLn $ "Listening on port " ++ show port
    run port (app js)


app :: Text
    -> Request -> (Response -> IO ResponseReceived) -> IO ResponseReceived
app js req respond = do
  print req
  ifElm
    (respond . indexResponse $ js)
    (case WAI.pathInfo req of
        ["getNumber"] -> randomIO >>= respond . changeNumberResponse
        _  -> respond badRequestResponse
    )
  
  where
    ifElm isNotElm isElm =
      case lookup "requestFromElm" (WAI.requestHeaders req) of
        Just _ -> isElm
        _ -> isNotElm
          
