{-#LANGUAGE BlockArguments #-}
module HtmlPages( indexHtml
                , notFoundHtml
                , badRequestHtml)
where

import Data.Text (Text)
import Text.Blaze ((!))
import Text.Blaze.Html5 (Html, toHtml)
import qualified Text.Blaze.Html5 as BLZ
import qualified Text.Blaze.Html5.Attributes as BLZ



indexHtml :: Text -> Html
indexHtml js =
  BLZ.docTypeHtml do BLZ.head do headScript
                     BLZ.body do elmDiv
                                 bodyScript
  where
    headScript =
      BLZ.script . BLZ.toHtml $ js

    elmDiv =
      BLZ.div ! BLZ.id (BLZ.stringValue "elm") $ toHtml ""

    bodyScript =
      BLZ.script . BLZ.toHtml $
      "var app = Elm.Main.init({node: document.getElementById('elm')});"
          

notFoundHtml :: Html
notFoundHtml =
  BLZ.docTypeHtml do BLZ.body do BLZ.p (BLZ.toHtml "404")


badRequestHtml :: Html
badRequestHtml =
  BLZ.docTypeHtml do BLZ.body do BLZ.p (BLZ.toHtml "400")
