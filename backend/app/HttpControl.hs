module HttpControl( indexResponse
                  , changeNumberResponse
                  , notFoundResponse
                  , badRequestResponse )
where

import HtmlPages

import Data.Int (Int32)

import Data.Text (Text)
import qualified Data.Text as TXT
import qualified Data.Text.Encoding as TENC

import qualified Data.Binary as BIN (encode)

import Network.HTTP.Types (status200, status202, status400, status404)
import Network.HTTP.Types.Header (hContentType)
import Network.Wai (Response, responseLBS)

import qualified Text.Blaze.Html.Renderer.Utf8 as BLZU



indexResponse :: Text -> Response
indexResponse js = 
  responseLBS status200 headers (BLZU.renderHtml (indexHtml js))
  where
    headers = [( hContentType
               , TENC.encodeUtf8 . TXT.pack $ "text/html" )]


changeNumberResponse :: Int32 -> Response
changeNumberResponse n =
  responseLBS status202 headers (BIN.encode n)
  where
    headers = [( hContentType
               , TENC.encodeUtf8 . TXT.pack $ "application/octet-stream" )]


notFoundResponse :: Response
notFoundResponse =
  responseLBS status404 headers (BLZU.renderHtml notFoundHtml)
  where
    headers = [( hContentType
               , TENC.encodeUtf8 . TXT.pack $ "text/html")]


badRequestResponse :: Response
badRequestResponse =
  responseLBS status400 headers (BLZU.renderHtml badRequestHtml)
  where
    headers = [( hContentType
               , TENC.encodeUtf8 . TXT.pack $ "text/html")]
