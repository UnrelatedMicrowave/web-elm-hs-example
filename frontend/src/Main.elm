module Main exposing (..)

import Url.Builder 

import Http
import Html exposing (Html, text, button, div, p)
import Html.Events exposing (onClick)
import Browser exposing (sandbox)
import Bytes as Bytes
import Bytes.Decode as BytesDec


type alias Model = { var1 : Int }

type Message = GenerateNumber
             | GotNumber (Result Http.Error Int)

    
main : Program () Model Message
main =
    Browser.element { init = init
                    , view = view
                    , update = update
                    , subscriptions = \_ -> Sub.none
                    }

        
init : () -> (Model, Cmd Message)
init _ = ({ var1 = 0 }, Cmd.none)

    
view : Model -> Html Message
view model =
    div [] [ p [] [text "generate random number"]
           , p [] [text <| String.fromInt model.var1]
           , button [onClick GenerateNumber] [text "generate"]
           ]


update : Message -> Model -> (Model, Cmd Message)
update msg m =
    case msg of
        GotNumber (Ok value) -> ({ var1 = value }, Cmd.none)
        GotNumber (Err _) -> ({ var1 = -1 }, Cmd.none)
        GenerateNumber -> (m, buttonRequest)


buttonRequest : Cmd Message
buttonRequest =
    let url = Url.Builder.relative
              ["getNumber"]
              []
        headers = [Http.header "requestFromElm" ""]
    in Http.request
        { method = "GET"
        , headers = headers
        , url = url
        , body = Http.emptyBody
        , expect = Http.expectBytes GotNumber decodeNumber 
        , timeout = Nothing
        , tracker = Nothing
        }


decodeNumber : BytesDec.Decoder Int
decodeNumber =
    BytesDec.signedInt32 Bytes.BE
