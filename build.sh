#!/bin/sh
cd frontend
elm make src/Main.elm --output=index.js
cd ..

cd backend
stack build
cp ../frontend/index.js .
cd ..


